const CACHE_NAME = 'atl-v2'
const assetsToCache = [
  '/',
  'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css',
  'https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js',
  'script.js',
]

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      cache.addAll(assetsToCache)
    })
  )
})

self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches.keys().then((keys) =>
      Promise.all(
        keys.map((key) => {
          if (key !== CACHE_NAME) {
            return caches.delete(key)
          }
        })
      )
    )
  )
})

self.addEventListener('fetch', (event) => {
  if (
    event.request.url !==
    'https://run.mocky.io/v3/18f867ec-5276-4a0c-8ae4-72a985eef462'
  ) {
    event.respondWith(
      fetch(event.request).catch(() => caches.match(event.request))
    )
  }
})
